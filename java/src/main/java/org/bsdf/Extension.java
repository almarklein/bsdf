package org.bsdf;

import java.util.Map;

public abstract class Extension
{
    protected String name;

    protected Extension(String name)
    {
        this.name = name;
    }

    public abstract boolean canEncode(Object o);

    public abstract Object decompose(Object o);

    public abstract Object compose(Object parts);
}
