package org.bsdf;

import org.bsdf.extensions.NDArrayExtension;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Data {

    public enum Types {
        Short('h', java.lang.Short.class),
        Long('i', java.lang.Long.class),
        Float('f', java.lang.Float.class),
        Double('d', java.lang.Double.class),
        String('s', java.lang.String.class),
        Blob('b', ByteBuffer.class, byte.class),
        List('l', Collection.class),
        Map('m', java.util.Map.class),
        Boolean(' ', Boolean.class),

        Null('v'), True('y'), False('n'),
        Array(' ', NDArrayExtension.supportedType.toArray(new Class[0])),
        Custom(' ');

        byte id;
        List<Class> types = new ArrayList<>();

        Types(char c, Class ... t) {
            id = (byte) c;
            types.addAll(Arrays.asList(t));
        }

        public boolean isInstance(Object o)
        {
            for (Class c : types)
            {
                if (c.isInstance(o))
                    return true;
            }
            return false;
        }
    }

    private static final int SHORT_LEN_CODE_MAX = 250;
    private static final int LONG_LEN_CODE =  253;
    private static final int CLOSED_STREAM = 254;
    private static final int OPEN_STREAM = 255;
    private static final byte[] HEADER = new byte[]{'B', 'S', 'D', 'F', 2, 2};
    private final ByteBuffer writeBuffer = ByteBuffer.allocate(Long.BYTES);
    private final ByteBuffer readBuffer = ByteBuffer.allocate(Long.BYTES);

    private OutputStream out;
    private InputStream in;

    private List<Extension> extensions;

    private List<Object> values = new ArrayList<>();
    private Iterator<Object> stream = null;

    public Data()
    {
        writeBuffer.order(ByteOrder.LITTLE_ENDIAN);
        readBuffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    protected Data(InputStream in, List<Extension> extensions) throws IOException {
        this();
        this.in = in;
        this.extensions = extensions;

        byte[] header = new byte[6];
        if (in.read(header) != header.length)
            throw new IOException("BSDF header not found - data is too short.");
        if (!Arrays.equals(header, HEADER))
            throw new IOException("Invalid BSDF header.");

        var ret = decode();
        if (ret instanceof List)
            values = (List<Object>) ret;
        else
            values.add(ret);
    }

    public Data add(List<Object> v) {
        values.add(v);
        return this;
    }

    public Data add(Map<String, Object> map) {
        values.add(map);
        return this;
    }

    public Data add(double d) {
        values.add(d);
        return this;
    }

    public Data add(float d) {
        values.add(d);
        return this;
    }

    public Data add(long l) {
        values.add(l);
        return this;
    }

    public Data add(boolean b) {
        values.add(b);
        return this;
    }

    public Data add(ByteBuffer bb) {
        values.add(bb);
        return this;
    }

    public Data add(boolean[] data) {
        values.add(data);
        return this;
    }

    public Data add(byte[] data) {
        values.add(ByteBuffer.wrap(data));
        return this;
    }

    public Data add(short[][] data) {
        values.add(data);
        return this;
    }

    public Data add(int[][] data) {
        values.add(data);
        return this;
    }

    public Data add(long[][] data) {
        values.add(data);
        return this;
    }

    public Data add(float[][] data) {
        values.add(data);
        return this;
    }

    public Data add(double[][] data) {
        values.add(data);
        return this;
    }

    public Data add(short[][][] data) {
        values.add(data);
        return this;
    }

    public Data add(int[][][] data) {
        values.add(data);
        return this;
    }

    public Data add(long[][][] data) {
        values.add(data);
        return this;
    }

    public Data add(float[][][] data) {
        values.add(data);
        return this;
    }

    public Data add(double[][][] data) {
        values.add(data);
        return this;
    }

    public Data add(Object ... o) {
        values.addAll(Arrays.asList(o));
        return this;
    }

    public Data addNull() {
        values.add(null);
        return this;
    }

    public Data setStream(Iterator<Object> stream)
    {
        this.stream = stream;
        return this;
    }

    public int size()
    {
        return values.size();
    }

    public Types getType(int i) {
        Object v = values.get(i);

        for (Types t : Types.values())
        {
            if (t.isInstance(v))
                return t;
        }

        if (v == null)
            return Types.Null;
        return Types.Custom;
    }

    public Object get(int i)
    {
        return values.get(i);
    }

    public Object remove(int i)
    {
        return values.remove(i);
    }

    private Object decode() throws IOException
    {
        char typeFull = (char)in.read();
        char type = Character.toLowerCase(typeFull);
        String ext_id = null;
        Object ret = null;

        if (type != typeFull)
            ext_id = decodeStr();

        if (type == Types.Null.id)
            return null;
        else if (type == Types.List.id)
        {
            long listLen = readLenCode();
            ArrayList<Object> list = new ArrayList<>();

            if (listLen == OPEN_STREAM)
            {
                if (in.skip(Long.BYTES) != Long.BYTES)
                    throw new IOException("Failed to advanced past stream size");
                while (in.available() > 0)
                    list.add(decode());
            }
            else if (listLen == CLOSED_STREAM)
            {
                in.read(readBuffer.array(), 0, Long.BYTES);
                listLen = readBuffer.clear().getLong();
            }

            if (listLen != OPEN_STREAM) {
                for (int n = 0; n < listLen; ++n)
                    list.add(decode());
            }
            ret = list;
        }
        else if (type == Types.Map.id)
        {
            long listLen = readLenCode();
            Map<String, Object> map = new HashMap<>();

            for (int n = 0; n < listLen; ++n)
                map.put(decodeStr(), decode());
            ret = map;
        }
        else if (type == Types.True.id)
            ret = true;
        else if (type == Types.False.id)
            ret = false;
        else if (type == Types.Short.id)
        {
            if (in.read(readBuffer.array(), 0, Short.BYTES) != Short.BYTES)
                throw new IOException("Failed to read short");
            ret = (long)readBuffer.position(0).getShort();
        }
        else if (type == Types.Long.id)
        {
            if (in.read(readBuffer.array(), 0, Long.BYTES) != Long.BYTES)
                throw new IOException("Failed to read long");
            ret = readBuffer.position(0).getLong();
        }
        else if (type == Types.Float.id)
        {
            if (in.read(readBuffer.array(), 0, Float.BYTES) != Float.BYTES)
                throw new IOException("Failed to read float");
            ret = readBuffer.position(0).getFloat();
        }
        else if (type == Types.Double.id)
        {
            if (in.read(readBuffer.array(), 0, Double.BYTES) != Double.BYTES)
                throw new IOException("Failed to read double");
            ret = readBuffer.position(0).getDouble();
        }
        else if (type == Types.String.id)
            ret = decodeStr();
        else if (type == Types.Blob.id)
        {
            long allocatedSize = readLenCode();
            long usedSize = readLenCode();
            long dataSize = readLenCode();
            if (in.read() != 0)
                throw new IOException("Compressed blobs are not supported");
            int hasCheckSum = in.read();

            if (hasCheckSum == 0xff && in.skip(16) != 16)
                throw new IOException("Failed to read Checksum value");
            else if (hasCheckSum != 0)
                throw new IOException("Blob's checksum existence value is invalid (should be 0 or 0xff): " + hasCheckSum);
            int alignment = in.read();
            if (alignment > 0 && in.skip(alignment) != alignment)
                throw new IOException("Failure reading block, could not align to byte boundary.");

            var data = new byte[(int)usedSize];
            int readSize = in.read(data);
            if (readSize != usedSize)
                throw new IOException("Failed to read all of blob. Required " + usedSize + ", read " + readSize);

            int skipSpace = (int)(allocatedSize - usedSize);
            if (in.skip(skipSpace) != skipSpace)
                throw new IOException("Failed to re-align blob data.");

            ret = ByteBuffer.wrap(data);
        }

        if (ret == null)
            throw new IOException("Unknown data type: " + type);

        if (ext_id != null)
        {
            for (Extension e : extensions)
            {
                if (ext_id.equals(e.name))
                    ret = e.compose(ret);
            }
        }

        return ret;
    }

    protected void write(OutputStream out, List<Extension> extensions) throws IOException {
        this.out = out;
        this.extensions = extensions;
        this.out.write(HEADER);


        if (stream != null && values.size() == 0)
        {
            encode(stream, null);
            return;
        }

        if (stream != null)
        {
            values.add(stream);
            encode(values, null);
            values.remove(values.size() - 1);
        }
        else
            encode(values, null);
    }

    private void encode(Object o, String ext_id) throws IOException
    {
        if (o == null)
            out.write(Types.Null.id);
        else if (o instanceof Boolean)
            out.write(Boolean.TRUE.equals(o) ? Types.True.id : Types.False.id);
        else if (o instanceof Short)
        {
            encodeTypeID(Types.Short, ext_id);
            writeBuffer.clear().putShort((Short)o).flip();
            out.write(writeBuffer.array(), 0, writeBuffer.limit());
        }
        else if (o instanceof Integer)
        {
            int v = (Integer)o;
            encode((long)v, ext_id);
        }
        else if (o instanceof Long)
        {
            long v = (Long)o;
            if (v >= Short.MIN_VALUE && v <= Short.MAX_VALUE)
                encode((short)v, null);
            else
            {
                encodeTypeID(Types.Long, ext_id);
                writeBuffer.clear().putLong((Long)o).flip();
                out.write(writeBuffer.array(), 0, writeBuffer.limit());
            }
        }
        else if (o instanceof Float)
        {
            encodeTypeID(Types.Float, ext_id);
            writeBuffer.clear().putFloat((Float)o).flip();
            out.write(writeBuffer.array(), 0, writeBuffer.limit());
        }
        else if (o instanceof Double)
        {
            encodeTypeID(Types.Double, ext_id);
            writeBuffer.clear().putDouble((Double)o).flip();
            out.write(writeBuffer.array(), 0, writeBuffer.limit());
        }
        else if (o instanceof String)
        {
            encodeTypeID(Types.String, ext_id);
            encodeStr((String)o);
        }
        else if (o instanceof ByteBuffer)
        {
            var bb = (ByteBuffer) o;
            encodeTypeID(Types.Blob, ext_id);
            int len = bb.limit() - bb.position();
            lenCode(len);
            lenCode(len);
            lenCode(len);
            out.write(0); //no compression
            out.write(0); //no CRC
            out.write(0); //no byte alignment
            out.write(bb.array(), bb.position(), len);
        }
        else if (o instanceof byte[])
        {
            encode(ByteBuffer.wrap((byte[])o), ext_id);
        }
        else if (o instanceof boolean[])
        {
            encodeTypeID(Types.List, ext_id);
            var arr = (boolean[])o;
            lenCode(arr.length);
            for (boolean n : arr)
                encode(n, null);
        }
        else if (o instanceof Collection)
        {
            var list = (Collection)o;
            encodeTypeID(Types.List, ext_id);
            lenCode(list.size());
            for (Object oo : list)
                encode(oo, null);
        }
        else if (o instanceof Map)
        {
            var map = (Map)o;
            encodeTypeID(Types.Map, ext_id);
            lenCode(map.size());
            for (var key : map.keySet())
            {
                encodeStr(key.toString());
                encode(map.get(key), null);
            }
        }
        else if (o instanceof Iterator)
        {
            encodeTypeID(Types.List, null);
            out.write(OPEN_STREAM);
            out.write(new byte[Long.BYTES]); //unknown length
            var it = (Iterator)o;
            while (it.hasNext())
                encode(it.next(), null);
        }
        else
        {
            boolean encoded = false;

            for (Extension e : extensions)
            {
                if (e.canEncode(o))
                {
                    encode(e.decompose(o), e.name);
                    encoded = true;
                }
            }

            if (!encoded)
                throw new IllegalArgumentException("Do not know how to encode: " + o);
        }

    }

    void encodeTypeID(Types type, String ext_type) throws IOException
    {
        if (ext_type == null)
            out.write(type.id);
        else
        {
            out.write((byte)Character.toUpperCase((char)type.id));
            encodeStr(ext_type);
        }
    }

    private void encodeStr(String str) throws IOException
    {
        var data = str.getBytes(StandardCharsets.UTF_8);
        lenCode(data.length);
        out.write(data);
    }

    private String decodeStr() throws IOException
    {
        byte[] data = new byte[(int)readLenCode()];
        if (in.read(data) != data.length)
            throw new IOException("Failed to read string");

        return new String(data, StandardCharsets.UTF_8);
    }

    private void lenCode(long len) throws IOException
    {
        if (len <= SHORT_LEN_CODE_MAX)
            out.write((byte)len);
        else
        {
            out.write(LONG_LEN_CODE);
            writeBuffer.clear().putLong(len).flip();
            out.write(writeBuffer.array(), 0, writeBuffer.limit());
        }
    }

    private long readLenCode() throws IOException
    {
        int len = in.read();

        if (len == LONG_LEN_CODE)
        {
            if (in.read(readBuffer.array()) != readBuffer.array().length)
                throw new IOException("Failed to read list length.");
            return readBuffer.position(0).getLong();
        }
        return len;
    }
}
