package org.bsdf.extensions;

import org.bsdf.Extension;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.*;

public class NDArrayExtension extends Extension {

    public static final Set<Class> supportedType = new HashSet<>();

    static {
        supportedType.add(short[].class);
        supportedType.add(int[].class);
        supportedType.add(long[].class);
        supportedType.add(float[].class);
        supportedType.add(double[].class);

        supportedType.add(short[][].class);
        supportedType.add(int[][].class);
        supportedType.add(long[][].class);
        supportedType.add(float[][].class);
        supportedType.add(double[][].class);

        supportedType.add(short[][][].class);
        supportedType.add(int[][][].class);
        supportedType.add(long[][][].class);
        supportedType.add(float[][][].class);
        supportedType.add(double[][][].class);
    }

    public NDArrayExtension()
    {
        super("ndarray");

    }

    @Override
    public boolean canEncode(Object o)
    {
        return supportedType.contains(o.getClass());
    }

    public Object decompose(Object o)
    {
        Map<String, Object> ret = new HashMap<>();

        if (o instanceof short[])
        {
            var arr = (short[])o;
            ret.put("shape", List.of(arr.length));
            ret.put("dtype", "uint16");
            var bb = ByteBuffer.allocate(Short.BYTES * arr.length).order(ByteOrder.LITTLE_ENDIAN);
            bb.asShortBuffer().put(arr);
            ret.put("data", bb.array());
        }
        else if (o instanceof short[][])
        {
            var arr = (short[][])o;
            ret.put("shape", List.of(arr.length, arr[0].length));
            ret.put("dtype", "uint16");
            var bb = ByteBuffer.allocate(Short.BYTES * arr.length * arr[0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asShortBuffer();
            for (short[] sub : arr)
                buff.put(sub);
            ret.put("data", bb.array());
        }
        else if (o instanceof short[][][])
        {
            var arr = (short[][][])o;
            ret.put("shape", List.of(arr.length, arr[0].length, arr[0][0].length));
            ret.put("dtype", "uint16");
            var bb = ByteBuffer.allocate(Short.BYTES * arr.length * arr[0].length * arr[0][0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asShortBuffer();
            for (short[][] sub : arr)
                for (short[] subsub : sub)
                    buff.put(subsub);
            ret.put("data", bb.array());
        }
        else if (o instanceof int[])
        {
            var arr = (int[])o;
            ret.put("shape", List.of(arr.length));
            ret.put("dtype", "uint32");
            var bb = ByteBuffer.allocate(Integer.BYTES * arr.length).order(ByteOrder.LITTLE_ENDIAN);
            bb.asIntBuffer().put(arr);
            ret.put("data", bb.array());
        }
        else if (o instanceof int[][])
        {
            var arr = (int[][])o;
            ret.put("shape", List.of(arr.length, arr[0].length));
            ret.put("dtype", "uint32");
            var bb = ByteBuffer.allocate(Integer.BYTES * arr.length * arr[0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asIntBuffer();
            for (int[] sub : arr)
                buff.put(sub);
            ret.put("data", bb.array());
        }
        else if (o instanceof int[][][])
        {
            var arr = (int[][][])o;
            ret.put("shape", List.of(arr.length, arr[0].length, arr[0][0].length));
            ret.put("dtype", "uint32");
            var bb = ByteBuffer.allocate(Integer.BYTES * arr.length * arr[0].length * arr[0][0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asIntBuffer();
            for (int[][] sub : arr)
                for (int[] subsub : sub)
                    buff.put(subsub);
            ret.put("data", bb.array());
        }
        else if (o instanceof long[])
        {
            var arr = (long[])o;
            ret.put("shape", List.of(arr.length));
            ret.put("dtype", "uint64");
            var bb = ByteBuffer.allocate(Long.BYTES * arr.length).order(ByteOrder.LITTLE_ENDIAN);
            bb.asLongBuffer().put(arr);
            ret.put("data", bb.array());
        }
        else if (o instanceof long[][])
        {
            var arr = (long[][])o;
            ret.put("shape", List.of(arr.length, arr[0].length));
            ret.put("dtype", "uint64");
            var bb = ByteBuffer.allocate(Long.BYTES * arr.length * arr[0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asLongBuffer();
            for (long[] sub : arr)
                buff.put(sub);
            ret.put("data", bb.array());
        }
        else if (o instanceof long[][][])
        {
            var arr = (long[][][])o;
            ret.put("shape", List.of(arr.length, arr[0].length, arr[0][0].length));
            ret.put("dtype", "uint64");
            var bb = ByteBuffer.allocate(Long.BYTES * arr.length * arr[0].length * arr[0][0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asLongBuffer();
            for (long[][] sub : arr)
                for (long[] subsub : sub)
                    buff.put(subsub);
            ret.put("data", bb.array());
        }
        else if (o instanceof float[])
        {
            var arr = (float[])o;
            ret.put("shape", List.of(arr.length));
            ret.put("dtype", "float32");
            var bb = ByteBuffer.allocate(Float.BYTES * arr.length).order(ByteOrder.LITTLE_ENDIAN);
            bb.asFloatBuffer().put(arr);
            ret.put("data", bb.array());
        }
        else if (o instanceof float[][])
        {
            var arr = (float[][])o;
            ret.put("shape", List.of(arr.length, arr[0].length));
            ret.put("dtype", "float32");
            var bb = ByteBuffer.allocate(Float.BYTES * arr.length * arr[0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asFloatBuffer();
            for (float[] sub : arr)
                buff.put(sub);
            ret.put("data", bb.array());
        }
        else if (o instanceof float[][][])
        {
            var arr = (float[][][])o;
            ret.put("shape", List.of(arr.length, arr[0].length, arr[0][0].length));
            ret.put("dtype", "float32");
            var bb = ByteBuffer.allocate(Float.BYTES * arr.length * arr[0].length * arr[0][0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asFloatBuffer();
            for (float[][] sub : arr)
                for (float[] subsub : sub)
                    buff.put(subsub);
            ret.put("data", bb.array());
        }
        else if (o instanceof double[])
        {
            var arr = (double[])o;
            ret.put("shape", List.of(arr.length));
            ret.put("dtype", "float64");
            var bb = ByteBuffer.allocate(Double.BYTES * arr.length).order(ByteOrder.LITTLE_ENDIAN);
            bb.asDoubleBuffer().put(arr);
            ret.put("data", bb.array());
        }
        else if (o instanceof double[][])
        {
            var arr = (double[][])o;
            ret.put("shape", List.of(arr.length, arr[0].length));
            ret.put("dtype", "float64");
            var bb = ByteBuffer.allocate(Double.BYTES * arr.length * arr[0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asDoubleBuffer();
            for (double[] sub : arr)
                buff.put(sub);
            ret.put("data", bb.array());
        }
        else if (o instanceof double[][][])
        {
            var arr = (double[][][])o;
            ret.put("shape", List.of(arr.length, arr[0].length, arr[0][0].length));
            ret.put("dtype", "float64");
            var bb = ByteBuffer.allocate(Double.BYTES * arr.length * arr[0].length * arr[0][0].length).order(ByteOrder.LITTLE_ENDIAN);
            var buff = bb.asDoubleBuffer();
            for (double[][] sub : arr)
                for (double[] subsub : sub)
                    buff.put(subsub);
            ret.put("data", bb.array());
        }

        return ret;
    }

    public Object compose(Object decomposed)
    {
        Map<String, Object> parts = (Map<String, Object>)decomposed;
        String type = parts.get("dtype").toString();
        List<Object> dimsList = (List<Object>)parts.get("shape");
        ByteBuffer bb = (ByteBuffer) parts.get("data");
        bb.order(ByteOrder.LITTLE_ENDIAN);

        int[] dims = new int[dimsList.size()];
        int idx = 0;
        for (Object o : dimsList)
        {
            if (o instanceof Short)
                dims[idx++] = (Short)o;
            if (o instanceof Long)
                dims[idx++] = ((Long)o).intValue();
        }

        switch (type) {
            case "uint16":
                if (dims.length == 1) {
                    short[] ret = new short[dims[0]];
                    bb.asShortBuffer().get(ret);
                    return ret;
                }
                else if (dims.length == 2)
                {
                    short[][] ret = new short[dims[0]][dims[1]];
                    var buff = bb.asShortBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        buff.get(ret[n]);
                    return ret;
                }
                else if (dims.length == 3)
                {
                    short[][][] ret = new short[dims[0]][dims[1]][dims[2]];
                    var buff = bb.asShortBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        for (int m = 0; m < dims[1]; ++m)
                            buff.get(ret[n][m]);
                    return ret;
                }
                break;
            case "uint32":
                if (dims.length == 1) {
                    int[] ret = new int[dims[0]];
                    bb.asIntBuffer().get(ret);
                    return ret;
                }
                else if (dims.length == 2)
                {
                    int[][] ret = new int[dims[0]][dims[1]];
                    var buff = bb.asIntBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        buff.get(ret[n]);
                    return ret;
                }
                else if (dims.length == 3)
                {
                    int[][][] ret = new int[dims[0]][dims[1]][dims[2]];
                    var buff = bb.asIntBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        for (int m = 0; m < dims[1]; ++m)
                            buff.get(ret[n][m]);
                    return ret;
                }
                break;
            case "uint64":
                if (dims.length == 1) {
                    long[] ret = new long[dims[0]];
                    bb.asLongBuffer().get(ret);
                    return ret;
                }
                else if (dims.length == 2)
                {
                    long[][] ret = new long[dims[0]][dims[1]];
                    var buff = bb.asLongBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        buff.get(ret[n]);
                    return ret;
                }
                else if (dims.length == 3)
                {
                    long[][][] ret = new long[dims[0]][dims[1]][dims[2]];
                    var buff = bb.asLongBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        for (int m = 0; m < dims[1]; ++m)
                            buff.get(ret[n][m]);
                    return ret;
                }
                break;
            case "float32":
                if (dims.length == 1) {
                    float[] ret = new float[dims[0]];
                    bb.asFloatBuffer().get(ret);
                    return ret;
                }
                else if (dims.length == 2)
                {
                    float[][] ret = new float[dims[0]][dims[1]];
                    var buff = bb.asFloatBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        buff.get(ret[n]);
                    return ret;
                }
                else if (dims.length == 3)
                {
                    float[][][] ret = new float[dims[0]][dims[1]][dims[2]];
                    var buff = bb.asFloatBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        for (int m = 0; m < dims[1]; ++m)
                            buff.get(ret[n][m]);
                    return ret;
                }
                break;
            case "float64":
                if (dims.length == 1) {
                    double[] ret = new double[dims[0]];
                    bb.asDoubleBuffer().get(ret);
                    return ret;
                }
                else if (dims.length == 2)
                {
                    double[][] ret = new double[dims[0]][dims[1]];
                    var buff = bb.asDoubleBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        buff.get(ret[n]);
                    return ret;
                }
                else if (dims.length == 3)
                {
                    double[][][] ret = new double[dims[0]][dims[1]][dims[2]];
                    var buff = bb.asDoubleBuffer();
                    for (int n = 0; n < dims[0]; ++n)
                        for (int m = 0; m < dims[1]; ++m)
                            buff.get(ret[n][m]);
                    return ret;
                }
                break;
        }

        throw new IllegalArgumentException("Unknown data type: " + type + ", or unsupported dimension count: " + dims.length);

    }
}
