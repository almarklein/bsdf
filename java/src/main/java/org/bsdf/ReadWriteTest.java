package org.bsdf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ReadWriteTest {

    public static void main(String[] args) throws IOException {
        Data data = new Data();
        var is = new int[100000];
        Arrays.fill(is, 1024 * 64);

        data.add(1);
        data.add(1024*64);
        data.add(2.0f);
        data.add(3.0);
        data.add(true);
        data.add(false);
        data.add(new int[10]);
        data.add(new int[10][20]);
        data.add("Hello there££");

        HashMap<String, Object> map = new HashMap<>();
        map.put("name1££", new int[10]);
        map.put("name2", new float[10]);
        map.put("name3", new float[100000]);
        map.put("name4", new double[100000]);
        map.put("name5", is);
        map.put("the blob", new byte[1024]);
        data.add(map);
        data.add("Next is Stream");

        ArrayList<Object> i = new ArrayList<>();
        for (int n = 0; n < 1000; ++n)
            i.add(n);
        data.setStream(i.iterator());

        ByteBuffer bb = (new Encoder()).encode(data);
        System.out.println(bb.limit());
        Data readBuff = (new Encoder()).decode(bb);

        Path disk = Path.of("test_data.bsdf");
        for (int n = 0; n < 5; ++n) {
            long s = System.nanoTime();
            var enc = new Encoder();
            data.setStream(i.iterator());
            enc.save(disk, data);
            System.out.printf("Write Time: %.3f s\n", (System.nanoTime() - s) * 1e-9);
        }


        for (int n = 0; n < 5; ++n) {
            long s = System.nanoTime();
            Data newData = (new Encoder()).load(disk);
            System.out.printf("Read Time: %.3f s\n", (System.nanoTime() - s) * 1e-9);
        }
    }

}
