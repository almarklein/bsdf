package org.bsdf;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class BSDFTest
{
    @Test
    public void testDataTypes()
    {
        Data data = new Data();

        data.add((long)1);
        assertEquals(Data.Types.Long, data.getType(0));

        data.add(1.0f);
        assertEquals(Data.Types.Float, data.getType(1));

        data.add(1.0);
        assertEquals(Data.Types.Double, data.getType(2));

        data.add("Hello££");
        assertEquals(Data.Types.String, data.getType(3));

        data.add(new byte[5]);
        assertEquals(Data.Types.Blob, data.getType(4));

        data.add(new ArrayList<>());
        assertEquals(Data.Types.List, data.getType(5));

        data.add(new HashMap<>());
        assertEquals(Data.Types.Map, data.getType(6));

        data.add((List)null);
        assertEquals(Data.Types.Null, data.getType(7));

        data.add(true);
        assertEquals(Data.Types.Boolean, data.getType(8));

        data.add(false);
        assertEquals(Data.Types.Boolean, data.getType(9));

        data.add(new int[3]);
        assertEquals(Data.Types.Array, data.getType(10));

        data.add(new double[3]);
        assertEquals(Data.Types.Array, data.getType(11));
    }

    @Test
    public void testIntWriting() throws IOException
    {
        Data data = new Data();

        data.add(0);
        data.add(1);
        data.add(-1);
        data.add(Short.MAX_VALUE);
        data.add(Short.MIN_VALUE);
        data.add(Short.MAX_VALUE + 1);
        data.add(Short.MIN_VALUE - 1);
        data.add(1024 * 64);
        data.add(-1024 * 64);
        data.add(Long.MAX_VALUE);
        data.add(Long.MIN_VALUE);

        var encoder = new Encoder();
        ByteBuffer bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        for (int n = 0; n < data.size(); ++n)
            assertEquals(data.get(n), out.get(n));
    }

    @Test
    public void testFloatWriting() throws IOException
    {
        Data data = new Data();

        data.add(0f);
        data.add(1f);
        data.add(-1f);
        data.add(100f);
        data.add(-100f);
        data.add(Float.MAX_VALUE);
        data.add(Float.MIN_VALUE);
        data.add(Float.NaN);

        var encoder = new Encoder();
        ByteBuffer bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        for (int n = 0; n < data.size(); ++n)
            assertEquals(data.get(n), out.get(n));
    }

    @Test
    public void testDoubleWriting() throws IOException
    {
        Data data = new Data();

        data.add(0.0);
        data.add(1.0);
        data.add(-1.0);
        data.add(100.0);
        data.add(-100.0);
        data.add(Double.MAX_VALUE);
        data.add(Double.MIN_VALUE);
        data.add(Double.NaN);

        var encoder = new Encoder();
        ByteBuffer bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        for (int n = 0; n < data.size(); ++n)
            assertEquals(data.get(n), out.get(n));
    }

    @Test
    public void testBooleanWriting() throws IOException
    {
        Data data = new Data();

        data.add(true);
        data.add(false);
        data.add(true);

        var encoder = new Encoder();
        ByteBuffer bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        for (int n = 0; n < data.size(); ++n)
            assertEquals(data.get(n), out.get(n));
    }

    @Test
    public void testStringWriting() throws IOException
    {
        Data data = new Data();

        data.add("");
        data.add("Hello");
        data.addNull();
        data.add("aA".repeat(1024 * 32));
        data.add("Hello2");

        var encoder = new Encoder();
        ByteBuffer bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        for (int n = 0; n < data.size(); ++n)
            assertEquals(data.get(n), out.get(n));
    }

    @Test
    public void testBlobWriting() throws IOException
    {
        Data data = new Data();
        data.add(new byte[10]);

        ByteBuffer outBB = ByteBuffer.allocate(Long.BYTES * 1024);
        for (int n = 0; n < 1024; ++n)
            outBB.putLong(n);
        var bbTmp = ByteBuffer.allocate(outBB.capacity() + 10);
        bbTmp.put(outBB.flip());
        data.add(bbTmp.flip());

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals(new byte[10], ((ByteBuffer)out.get(0)).array());
        assertArrayEquals(outBB.array(), ((ByteBuffer)out.get(1)).array());
    }

    @Test
    public void testMapWriting() throws IOException
    {
        Data data = new Data();
        Map<String, Object> map1 = new HashMap<>();
        map1.put("a", 1);
        map1.put("b", 1.0);
        map1.put("c", true);
        map1.put("d", new int[1024]);
        data.add(map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("a", 2);
        map2.put("b", 2.0);
        map2.put("c", false);
        map2.put("d", new ArrayList<>());
        data.add(map2);

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        compareMaps(map1, (Map<String, Object>)out.get(0));
        compareMaps(map2, (Map<String, Object>)out.get(1));
    }


    private void compareMaps(Map<String, Object> map1, Map<String, Object> omap)
    {
        for (String key : map1.keySet())
        {
            if (map1.get(key) instanceof Integer)
                assertEquals(((Integer)map1.get(key)).intValue(), ((Long)omap.get(key)).intValue());
            else if (map1.get(key) instanceof int[])
                assertArrayEquals((int[])map1.get(key), (int[])omap.get(key));
            else
                assertEquals(map1.get(key), omap.get(key));
            omap.remove(key);
        }
        assertEquals(0, omap.size());
    }

    @Test
    public void testNDArrayInts() throws IOException
    {
        Random r = new Random();
        Data data = new Data();
        var s = new short[1024];
        var sl = new short[1024 * 33];
        s[0] = Short.MIN_VALUE;
        s[1] = Short.MAX_VALUE;
        data.add(s, sl);

        for (int n = 2; n < s.length; ++n)
            s[n] = (short)(r.nextInt(Short.MAX_VALUE) * (n % 2 == 0 ? 1 : -1));
        for (int n = 0; n < sl.length; ++n)
            sl[n] = (short)(r.nextInt(Short.MAX_VALUE) * (n % 2 == 0 ? 1 : -1));

        var i = new int[1024];
        var il = new int[1024 * 33];
        i[0] = Integer.MIN_VALUE;
        i[1] = Integer.MAX_VALUE;
        data.add(i, il);

        for (int n = 2; n < s.length; ++n)
            i[n] = (r.nextInt() * (n % 2 == 0 ? 1 : -1));
        for (int n = 0; n < sl.length; ++n)
            il[n] = (r.nextInt() * (n % 2 == 0 ? 1 : -1));

        var l = new long[1024];
        var ll = new long[1024 * 33];
        l[0] = Long.MIN_VALUE;
        l[1] = Long.MAX_VALUE;
        data.add(l, ll);

        for (int n = 2; n < s.length; ++n)
            l[n] = (r.nextLong() * (n % 2 == 0 ? 1 : -1));
        for (int n = 0; n < sl.length; ++n)
            ll[n] = (r.nextInt() * (n % 2 == 0 ? 1 : -1));

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals((short[])data.get(0), (short[])out.get(0));
        assertArrayEquals((short[])data.get(1), (short[])out.get(1));
        assertArrayEquals((int[])data.get(2), (int[])out.get(2));
        assertArrayEquals((int[])data.get(3), (int[])out.get(3));
        assertArrayEquals((long[])data.get(4), (long[])out.get(4));
        assertArrayEquals((long[])data.get(5), (long[])out.get(5));
    }

    @Test
    public void test2DNDArrayInts() throws IOException
    {
        Random r = new Random();
        Data data = new Data();
        var sl = new short[10][1024 * 33];
        data.add(sl);

        for (int n = 0; n < sl.length; ++n)
            for (int m = 0; m < sl[0].length; ++m)
                sl[n][m] = (short)r.nextInt(Short.MAX_VALUE);

        var il = new int[10][1024 * 33];
        data.add(il);

        for (int n = 0; n < il.length; ++n)
            il[n] = r.ints(il[n].length).toArray();

        var ll = new long[10][1024 * 33];
        data.add(ll);

        for (int n = 0; n < ll.length; ++n)
            ll[n] = r.longs(ll[n].length).toArray();

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals((short[][])data.get(0), (short[][])out.get(0));
        assertArrayEquals((int[][])data.get(1), (int[][])out.get(1));
        assertArrayEquals((long[][])data.get(2), (long[][])out.get(2));
    }

    @Test
    public void test3DNDArrayInts() throws IOException
    {
        Random r = new Random();
        Data data = new Data();
        var sl = new short[10][10][1024 * 33];
        data.add(sl);

        for (int n = 0; n < sl.length; ++n)
            for (int m = 0; m < sl[0].length; ++m)
                for (int p = 0; p < sl[0][0].length; ++p)
                    sl[n][m][p] = (short)r.nextInt(Short.MAX_VALUE);

        var il = new int[10][10][1024 * 33];
        data.add(il);

        for (int n = 0; n < il.length; ++n)
            for (int m = 0; m < il[0].length; ++m)
                il[n][m] = r.ints(il[n].length).toArray();

        var ll = new long[10][10][1024 * 33];
        data.add(ll);

        for (int n = 0; n < ll.length; ++n)
            for (int m = 0; m < ll[0].length; ++m)
                ll[n][m] = r.longs(ll[n].length).toArray();

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals((short[][][])data.get(0), (short[][][])out.get(0));
        assertArrayEquals((int[][][])data.get(1), (int[][][])out.get(1));
        assertArrayEquals((long[][][])data.get(2), (long[][][])out.get(2));
    }

    @Test
    public void testNDArrayFloat() throws IOException
    {
        Random r = new Random();
        Data data = new Data();
        var s = new float[1024];
        var sl = new float[1024 * 33];
        s[0] = Float.MIN_VALUE;
        s[1] = Float.MAX_VALUE;
        s[2] = Float.NEGATIVE_INFINITY;
        s[3] = Float.POSITIVE_INFINITY;
        s[4] = Float.NaN;
        data.add(s, sl);

        for (int n = 5; n < s.length; ++n)
            s[n] = r.nextFloat() * (n % 2 == 0 ? 1 : -1);
        for (int n = 0; n < sl.length; ++n)
            sl[n] = r.nextFloat() * (n % 2 == 0 ? 1 : -1);

        var d = new double[1024];
        var dl = new double[1024 * 33];
        d[0] = Double.MIN_VALUE;
        d[1] = Double.MAX_VALUE;
        d[2] = Double.NEGATIVE_INFINITY;
        d[3] = Double.POSITIVE_INFINITY;
        d[4] = Double.NaN;
        data.add(d, dl);

        for (int n = 5; n < s.length; ++n)
            d[n] = r.nextDouble() * (n % 2 == 0 ? 1 : -1);
        for (int n = 0; n < sl.length; ++n)
            dl[n] = r.nextDouble() * (n % 2 == 0 ? 1 : -1);

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals((float[])data.get(0), (float[])out.get(0));
        assertArrayEquals((float[])data.get(1), (float[])out.get(1));
        assertArrayEquals((double[])data.get(2), (double[])out.get(2));
        assertArrayEquals((double[])data.get(3), (double[])out.get(3));
    }

    @Test
    public void test2DNDArrayFloat() throws IOException
    {
        Random r = new Random();
        Data data = new Data();
        var fl = new float[10][1024 * 33];
        data.add(fl);

        for (int n = 0; n < fl.length; ++n)
            for (int m = 0; m < fl[0].length; ++m)
                fl[n][m] = r.nextFloat();

        var dl = new double[10][1024 * 33];
        data.add(dl);

        for (int n = 0; n < dl.length; ++n)
            dl[n] = r.doubles(dl[0].length).toArray();

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals((float[][])data.get(0), (float[][])out.get(0));
        assertArrayEquals((double[][])data.get(1), (double[][])out.get(1));
    }

    @Test
    public void test3DNDArrayFloat() throws IOException
    {
        Random r = new Random();
        Data data = new Data();
        var fl = new float[10][10][1024 * 33];
        data.add(fl);

        for (int n = 0; n < fl.length; ++n)
            for (int m = 0; m < fl[0].length; ++m)
                for (int p = 0; p < fl[0][0].length; ++p)
                    fl[n][m][p] = r.nextFloat();

        var dl = new double[10][10][1024 * 33];
        data.add(dl);

        for (int n = 0; n < dl.length; ++n)
            for (int m = 0; m < dl[0].length; ++m)
                dl[n][m] = r.doubles(dl[0].length).toArray();

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        assertArrayEquals((float[][][])data.get(0), (float[][][])out.get(0));
        assertArrayEquals((double[][][])data.get(1), (double[][][])out.get(1));
    }

    @Test
    public void testBooleanArray() throws IOException
    {
        boolean[] barr = new boolean[3];
        barr[0] = true;
        barr[1] = true;
        barr[2] = false;

        var data = new Data();
        data.add(barr);

        var encoder = new Encoder();
        var bb = encoder.encode(data);
        Data out = encoder.decode(bb);

        List<Object> bools = (List<Object>)out.get(0);
        for (int n = 0; n < barr.length; ++n)
            assertEquals(barr[n], bools.get(n));
    }

    @Test
    public void testErrors()
    {
        //Ensure there is an exception when we don't know how to encode
        assertThrows(IllegalArgumentException.class, () -> {
            Data data = new Data();
            data.add(new Data());
            new Encoder().encode(data);
        });

        //Ensure there is an exception when we don't know how to encode
        assertThrows(IOException.class, () -> {
            Data data = new Data();
            data.add(1);
            ByteBuffer bb = new Encoder().encode(data);
            bb.put(0, (byte)0); // corrupt the header
            new Encoder().decode(bb);
        });

        //Ensure there is an exception when we don't know how to encode
        assertThrows(IOException.class, () -> {
            new Encoder().decode(ByteBuffer.allocate(1));
        });
    }
}
